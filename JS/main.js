const BASE_URL = "https://633ec0f583f50e9ba3b76fe2.mockapi.io/";
var studentList = [];
var fetchListStudentService = function () {
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (response) {
      renderStudentList(response.data);
    })
    .catch(function (error) {
      console.log("error: ", error);
    });
};
fetchListStudentService();
var renderStudentList = function (listStu) {
  var contentHTML = "";
  studentList = listStu.map(function (currentStu) {
    return new Student(
      currentStu.id,
      currentStu.name,
      currentStu.email,
      currentStu.password,
      currentStu.mathPoint,
      currentStu.physPoint,
      currentStu.chemPoint
    );
  });
  studentList.forEach(function (student) {
    contentHTML += `<tr>
                <td>${student.id}</td>
                <td>${student.name}</td>
                <td>${student.email}</td>
                 <td>${student.countAverage()}</td>
                <td>
                <button onclick="editStudent('${
                  student.id
                }')" class="btn btn-primary">Sửa</button>
                <button onclick="deleteStudent(${
                  student.id
                })" class="btn btn-danger">Xoá</button>
                </td>
              </tr>`;
  });

  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};
var deleteStudent = function (idStu) {
  axios({
    url: `${BASE_URL}/sv/${idStu}`,
    method: "DELETE",
  })
    .then(function (res) {
      fetchListStudentService();
      Swal.fire("Xoá thành công");
      console.log("res: ", res);
    })
    .catch(function (err) {
      Swal.fire("Xoá thất bại");

      console.log("err: ", err);
    });
};
var addStudent = function () {
  var sv = getData();

  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: sv,
  })
    .then(function (res) {
      Swal.fire("Thêm thành công");
      fetchListStudentService();
    })
    .catch(function (err) {
      Swal.fire("Thêm thất bại");
    });
};
var editStudent = function (idStu) {
  document.getElementById("txtMaSV").disabled = true;

  axios
    .get(`${BASE_URL}/sv/${idStu}`)
    .then(function (res) {
      console.log("res: ", res.data);
      // gọi hàm show thông tin lên form
      showData(res.data);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
};
var updateStudent = function () {
  var sv = getData();
  axios({
    url: `${BASE_URL}/sv/${sv.id}`,
    method: "PUT",
    data: sv,
  })
    .then(function (res) {
      console.log("res: ", res.data);
      fetchListStudentService();
      Swal.fire("Sửa thành công");
    })
    .catch(function (err) {
      console.log("err: ", err);
      Swal.fire("Sửa thất bại");
    });
  document.getElementById("txtMaSV").disabled = false;
  resetForm();
};
